package pl.vashrax.myappapm;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import pl.vashrax.myappapm.db.DatabaseHelper;
import pl.vashrax.myappapm.model.Cabinet;
import pl.vashrax.myappapm.model.User;
import pl.vashrax.myappapm.model.Wire;
import pl.vashrax.myappapm.utils.DataCreator;
import pl.vashrax.myappapm.utils.VariableContainer;

import java.util.ArrayList;
import java.util.List;

public class CabinetMenu extends AppCompatActivity implements View.OnClickListener {

    private TextView menuCabinettxtViewWireCode, menuCabinettxtViewUserNumber, menuCabinettxtViewUserFirstName, menuCabinettxtViewUserLastName, menuCabinettxtViewWiresCount;
    private List<Wire> wiresForCabinet = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private DatabaseHelper databaseHelper;
    private int actualWireId = 0;
    private int cabinet_id = 1;
    private String cabinet_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cabinet);
        databaseHelper = new DatabaseHelper(this);

        Intent intent = getIntent();

        cabinet_code = intent.getStringExtra(VariableContainer.CABINET_CODE);
        cabinet_id = intent.getIntExtra(VariableContainer.CABINET_ID, 0);

        reloadDb();

        Button menuCabinetButtonExit = (Button) this.findViewById(R.id.menuCabinetButtonExit);
        menuCabinetButtonExit.setOnClickListener(this);

        Button menuCabinetButtonEdit = (Button) this.findViewById(R.id.menuCabinetButtonEdit);
        menuCabinetButtonEdit.setOnClickListener(this);

        Button menuCabinetButtonAddNew = (Button) this.findViewById(R.id.menuCabinetButtonAddNew);
        menuCabinetButtonAddNew.setOnClickListener(this);

        Button menuCabinetButtonPrev = (Button) this.findViewById(R.id.menuCabinetButtonPrev);
        menuCabinetButtonPrev.setOnClickListener(this);

        Button menuCabinetButtonNext = (Button) this.findViewById(R.id.menuCabinetButtonNext);
        menuCabinetButtonNext.setOnClickListener(this);

        Button menuCabinetButtonDelete = (Button) this.findViewById(R.id.menuCabinetButtonDelete);
        menuCabinetButtonDelete.setOnClickListener(this);

        TextView menuCabinetTextViewCode = this.findViewById(R.id.menuCabinetTextViewCode);
        menuCabinetTextViewCode.setText((CharSequence) String.valueOf(cabinet_code));

        menuCabinettxtViewWireCode = this.findViewById(R.id.menuCabinetTextViewWireCode);
        menuCabinettxtViewUserNumber = this.findViewById(R.id.menuCabinetTextViewUserNumber);
        menuCabinettxtViewUserFirstName = this.findViewById(R.id.menuCabinetTextViewUserFirstName);
        menuCabinettxtViewUserLastName = this.findViewById(R.id.menuCabinetTextViewUserLastName);
        menuCabinettxtViewWiresCount = this.findViewById(R.id.menuCabinetTextViewWireCount);

        showWire(actualWireId);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.menuCabinetButtonExit) {
            this.finish();
        }
        if (view.getId() == R.id.menuCabinetButtonEdit) {
            Wire wire = wiresForCabinet.get(actualWireId);
            Intent intent = new Intent(this, WireEdit.class);
            intent.putExtra(VariableContainer.IS_NEW, 0);
            intent.putExtra(VariableContainer.CABINET_ID, cabinet_id);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinet_code);
            intent.putExtra(VariableContainer.WIRE_CODE, menuCabinettxtViewWireCode.getText().toString());
            intent.putExtra(VariableContainer.WIRE_ID, actualWireId);
            intent.putExtra(VariableContainer.USER_NUMBER, wire.getUser().getNumber());
            intent.putExtra(VariableContainer.USER_FIRST_NAME, wire.getUser().getFirstName());
            intent.putExtra(VariableContainer.USER_LAST_NAME, wire.getUser().getLastName());
            startActivity(intent);
        }
        if (view.getId() == R.id.menuCabinetButtonAddNew) {
            Intent intent = new Intent(this, WireEdit.class);
            intent.putExtra(VariableContainer.IS_NEW, 1);
            intent.putExtra(VariableContainer.WIRE_CODE, DataCreator.generateCode(9));
            startActivity(intent);
        }
        if (view.getId() == R.id.menuCabinetButtonPrev) {
            if (actualWireId == 0) {

            } else {
                actualWireId--;
                showWire(actualWireId);
            }
        }
        if (view.getId() == R.id.menuCabinetButtonNext) {
            if (actualWireId >= (wiresForCabinet.size()-1)) {

            } else {
                actualWireId++;
                showWire(actualWireId);
            }
        }
        if (view.getId() == R.id.menuCabinetButtonDelete) {
            databaseHelper.deleteWire(wiresForCabinet.get(actualWireId).getCode());
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showWire (int wireId){
        Wire wire = wiresForCabinet.get(wireId);
        menuCabinettxtViewWireCode.setText(wire.getCode());
        menuCabinettxtViewUserNumber.setText(wire.getUser().getNumber());
        menuCabinettxtViewUserFirstName.setText(wire.getUser().getFirstName());
        menuCabinettxtViewUserLastName.setText(wire.getUser().getLastName());
        menuCabinettxtViewWiresCount.setText((actualWireId + 1) + "/" + wiresForCabinet.size());
    }

    private void reloadDb() {

        Cursor cursorCabinets = databaseHelper.findAllFromCabinets();
        Cabinet cabinet = null;

        while (cursorCabinets.moveToNext()) {
            long nr = cursorCabinets.getLong(0);
            String code = cursorCabinets.getString(1);
            if (cabinet_id == nr) {
                cabinet = new Cabinet(nr, code);
            }
        }
        cursorCabinets.close();

        users = new ArrayList<>();

        Cursor cursorUsers = databaseHelper.findAllFromUsers();
        User user;
        while (cursorUsers.moveToNext()) {
            long nr = cursorUsers.getLong(0);
            String number = cursorUsers.getString(1);
            String firstName = cursorUsers.getString(2);
            String lastName = cursorUsers.getString(3);
            users.add(new User(nr, number, firstName, lastName));
        }
        cursorUsers.close();

        wiresForCabinet = new ArrayList<>();

        Cursor cursorWires = databaseHelper.findAllFromWires();
        while (cursorWires.moveToNext()) {
            long nr = cursorWires.getLong(0);
            String code = cursorWires.getString(1);
            int cabinetId = cursorWires.getInt(2);
            int userId = cursorWires.getInt(3);
            if (cabinetId == cabinet_id) {
                User wireUser = databaseHelper.findUserById(userId+1);
                if(wireUser == null)
                    wireUser = databaseHelper.findUserById(userId);
                wiresForCabinet.add(new Wire(nr, code, cabinet, wireUser));
            }
        }
        cursorWires.close();

    }
}
