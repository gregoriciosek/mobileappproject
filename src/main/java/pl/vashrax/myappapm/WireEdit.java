package pl.vashrax.myappapm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;
import pl.vashrax.myappapm.db.DatabaseHelper;
import pl.vashrax.myappapm.model.User;
import pl.vashrax.myappapm.utils.VariableContainer;

public class WireEdit extends Activity implements View.OnClickListener {

    private int isNew = 0;
    private DatabaseHelper db = new DatabaseHelper(this);

    private EditText editTextUserNumber;
    private EditText editTextUserFirstName;
    private EditText editTextUserLastName;
    private Toast toast;
    private String wireCode;
    private int cabinetId;
    private String cabinetCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_wire);

        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        Intent intent = getIntent();
        isNew = intent.getIntExtra(VariableContainer.IS_NEW, 0);
        cabinetId = intent.getIntExtra(VariableContainer.CABINET_ID, 1);
        cabinetCode = intent.getStringExtra(VariableContainer.CABINET_CODE);
        wireCode = intent.getStringExtra(VariableContainer.WIRE_CODE);
        String userNumber = intent.getStringExtra(VariableContainer.USER_NUMBER);
        String userFirstName = intent.getStringExtra(VariableContainer.USER_FIRST_NAME);
        String userLastName = intent.getStringExtra(VariableContainer.USER_LAST_NAME);

        Button editWireButtonExit = (Button) this.findViewById(R.id.editWireButtonExit);
        editWireButtonExit.setOnClickListener(this);
        Button editWireButtonSave = (Button) this.findViewById(R.id.editWireButtonSave);
        editWireButtonSave.setOnClickListener(this);

        TextView editWiretextViewWireCode = (TextView) this.findViewById(R.id.editWireTextViewWireCode);
        TextView editWireTextViewCabinetCode = (TextView) this.findViewById(R.id.editWireTextViewCabinetCode) ;
        editTextUserNumber = (EditText) this.findViewById(R.id.editWireEditTextUserNumber);
        if(isNew == 0)
            editTextUserNumber.setEnabled(false);
        editTextUserFirstName = (EditText) this.findViewById(R.id.editWireEditTextUserFirstName);
        editTextUserLastName = (EditText) this.findViewById(R.id.editWireEditTextUserLastName);
        editWiretextViewWireCode.setText(wireCode);
        editWireTextViewCabinetCode.setText(cabinetCode);

        if(isNew == 1) {
            editTextUserLastName.setText("");
            editTextUserFirstName.setText("");
            editTextUserNumber.setText("");
        } else if (isNew == 0) {
            editTextUserLastName.setText(userLastName);
            editTextUserFirstName.setText(userFirstName);
            editTextUserNumber.setText(userNumber);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.editWireButtonExit) {
            this.finish();
        } else if(view.getId() == R.id.editWireButtonSave) {
            try {
                String number = "";
                String firstName = "";
                String lastName = "";
                number = editTextUserNumber.getText().toString();
                firstName = editTextUserFirstName.getText().toString().toString();
                lastName = editTextUserLastName.getText().toString();
                User user = new User(number,firstName,lastName);
                if (isNew == 0) {
                    db.updateUser(user.getNumber(), user.getFirstName(), user.getLastName());
                    showToastWithMessage("Zapisano zmiany.");
                } else {
                    db.addNewUser(user.getNumber(), user.getFirstName(), user.getLastName());
                    db.addNewWire(wireCode, cabinetId, db.getUserId(user.getNumber()));
                    showToastWithMessage("Dodano nowy przewód.");
                }
                onDestroy();
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void showToastWithMessage(String message){
        toast.setText(message);
        toast.show();
    }

}