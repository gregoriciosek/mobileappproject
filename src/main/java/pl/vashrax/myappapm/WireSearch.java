package pl.vashrax.myappapm;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pl.vashrax.myappapm.db.DatabaseHelper;
import pl.vashrax.myappapm.model.Cabinet;
import pl.vashrax.myappapm.model.User;
import pl.vashrax.myappapm.model.Wire;
import pl.vashrax.myappapm.utils.VariableContainer;

public class WireSearch extends Activity implements View.OnClickListener {

    private EditText editTextWireCode;
    private DatabaseHelper databaseHelper;

    private long wire_id = 0;
    private String wire_code = "";
    private int cabinet_id = 0;
    private int user_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_wire);

        databaseHelper = new DatabaseHelper(this);

        Button btnSearch = (Button) this.findViewById(R.id.searchWireButtonFind);
        btnSearch.setOnClickListener(this);

        Button btnExit = (Button) this.findViewById(R.id.searchWireButtonExit);
        btnExit.setOnClickListener(this);

        editTextWireCode = (EditText) this.findViewById(R.id.searchWireEditTextWireCode);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.searchWireButtonExit) {
            this.finish();
        } else if (view.getId() == R.id.searchWireButtonFind) {
            Intent intent = new Intent(this, WireEdit.class);

            Wire wire = null;
            User user = null;
            Cabinet cabinet = null;

            Cursor cursorWire = databaseHelper.findAllFromWiresWhereCode(editTextWireCode.getText().toString());

            while(cursorWire.moveToNext()) {
                        wire_id = cursorWire.getLong(0);
                        wire_code = cursorWire.getString(1);
                        cabinet_id = cursorWire.getInt(2);
                        user_id = cursorWire.getInt(3);


                System.out.println(wire_id);
                System.out.println(wire_code);
                System.out.println(user_id);
                System.out.println(cabinet_id);

            }

            Cursor cursorUser = databaseHelper.findAllFromUsersWhereId(user_id);

            while(cursorUser.moveToNext()) {
                user = new User(
                        cursorUser.getLong(0),
                        cursorUser.getString(1),
                        cursorUser.getString(2),
                        cursorUser.getString(3));
                System.out.println(user);
            }

            Cursor cursorCabinet = databaseHelper.findAllFromCabinetsWhereId(cabinet_id+1);

            while(cursorCabinet.moveToNext()) {
                cabinet = new Cabinet(
                        cursorCabinet.getLong(0),
                        cursorCabinet.getString(1));
                System.out.println(cabinet);
            }

            wire = new Wire(wire_id, wire_code, cabinet, user);

            intent.putExtra(VariableContainer.IS_NEW, 0);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinet.getCode());
            intent.putExtra(VariableContainer.WIRE_CODE, wire.getCode());
            intent.putExtra(VariableContainer.WIRE_ID, (int) wire_id);
            intent.putExtra(VariableContainer.USER_NUMBER, user.getNumber());
            intent.putExtra(VariableContainer.USER_FIRST_NAME, user.getFirstName());
            intent.putExtra(VariableContainer.USER_LAST_NAME, user.getLastName());

            startActivity(intent);
        }
    }
}
