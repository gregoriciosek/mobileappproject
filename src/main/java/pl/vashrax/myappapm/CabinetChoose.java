package pl.vashrax.myappapm;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pl.vashrax.myappapm.db.DatabaseHelper;
import pl.vashrax.myappapm.model.Cabinet;
import pl.vashrax.myappapm.utils.VariableContainer;

import java.util.ArrayList;
import java.util.List;

public class CabinetChoose extends Activity implements View.OnClickListener {


    private DatabaseHelper databaseHelper;
    private List<Cabinet> cabinetList = new ArrayList<Cabinet>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_cabinet);

        Button chooseCabinetButtonFirst = (Button) this.findViewById(R.id.chooseCabinetButtonFirst);
        Button chooseCabinetButtonSecond = (Button) this.findViewById(R.id.chooseCabinetButtonSecond);
        Button chooseCabinetButtonThird = (Button) this.findViewById(R.id.chooseCabinetButtonThird);
        Button chooseCabinetButtonFourth = (Button) this.findViewById(R.id.chooseCabinetButtonFourth);
        Button chooseCabinetButtonFifth = (Button) this.findViewById(R.id.chooseCabinetButtonFifth);
        Button chooseCabinetButtonExit = (Button) this.findViewById(R.id.chooseCabinetButtonExit);

        chooseCabinetButtonFirst.setOnClickListener(this);
        chooseCabinetButtonSecond.setOnClickListener(this);
        chooseCabinetButtonThird.setOnClickListener(this);
        chooseCabinetButtonFourth.setOnClickListener(this);
        chooseCabinetButtonFifth.setOnClickListener(this);
        chooseCabinetButtonExit.setOnClickListener(this);

        databaseHelper = new DatabaseHelper(this);
        Cursor cursor = databaseHelper.findAllFromCabinets();
        while(cursor.moveToNext()){
            long nr = cursor.getLong(0);
            String code = cursor.getString(1);
            cabinetList.add(new Cabinet(nr, code));
        }

        chooseCabinetButtonFirst.setText("Szafa 1 : " + cabinetList.get(0).getCode());
        chooseCabinetButtonSecond.setText("Szafa 2 : " + cabinetList.get(1).getCode());
        chooseCabinetButtonThird.setText("Szafa 3 : " + cabinetList.get(2).getCode());
        chooseCabinetButtonFourth.setText("Szafa 4 : " + cabinetList.get(3).getCode());
        chooseCabinetButtonFifth.setText("Szafa 5 : " + cabinetList.get(4).getCode());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.chooseCabinetButtonExit) {
            this.finish();
        } else if (view.getId() == R.id.chooseCabinetButtonFirst) {
            Intent intent = new Intent (this, CabinetMenu.class);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinetList.get(0).getCode());
            intent.putExtra(VariableContainer.CABINET_ID, 1);
            startActivity(intent);
        } else if (view.getId() == R.id.chooseCabinetButtonSecond) {
            Intent intent = new Intent (this, CabinetMenu.class);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinetList.get(1).getCode());
            intent.putExtra(VariableContainer.CABINET_ID, 2);
            startActivity(intent);
        } else if (view.getId() == R.id.chooseCabinetButtonThird) {
            Intent intent = new Intent (this, CabinetMenu.class);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinetList.get(2).getCode());
            intent.putExtra(VariableContainer.CABINET_ID, 3);
            startActivity(intent);
        } else if (view.getId() == R.id.chooseCabinetButtonFourth) {
            Intent intent = new Intent (this, CabinetMenu.class);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinetList.get(3).getCode());
            intent.putExtra(VariableContainer.CABINET_ID, 4);
            startActivity(intent);
        } else if (view.getId() == R.id.chooseCabinetButtonFifth) {
            Intent intent = new Intent (this, CabinetMenu.class);
            intent.putExtra(VariableContainer.CABINET_CODE, cabinetList.get(4).getCode());
            intent.putExtra(VariableContainer.CABINET_ID, 5);
            startActivity(intent);
        }
    }

}
