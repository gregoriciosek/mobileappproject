package pl.vashrax.myappapm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Wire {

    private Long id;
    private String code;
    private Cabinet cabinet;
    private User user;

}
