package pl.vashrax.myappapm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pl.vashrax.myappapm.db.DatabaseHelper;
import pl.vashrax.myappapm.model.User;
import pl.vashrax.myappapm.utils.DataCreator;

public class MainActivity extends Activity implements View.OnClickListener {

    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button menuMainButtonFirst = (Button) this.findViewById(R.id.menuMainButtonCabinets);
        Button menuMainButtonExit = (Button) this.findViewById(R.id.menuMainButtonExit);
        Button menuMainButtonFindWire = (Button) this.findViewById(R.id.menuMainButtonFindWire);
        menuMainButtonFirst.setOnClickListener(this);
        menuMainButtonExit.setOnClickListener(this);
        menuMainButtonFindWire.setOnClickListener(this);

        databaseHelper = new DatabaseHelper(this);

        databaseHelper.deleteTables();
        databaseHelper.createTables();

        databaseHelper.addNewCabinet("cvk124b1vrkt2v41k2v4");
        databaseHelper.addNewCabinet("asdfgh2sadgh4taqashg");
        databaseHelper.addNewCabinet("asdhfjkjhgwe34567832");
        databaseHelper.addNewCabinet("c12345678ikjhgfd34rt");
        databaseHelper.addNewCabinet("dfgfhjktyrter3245667");

        for(int i=0 ;i<100;i++) {
            User user = new User((long) i+1, DataCreator.generateNumber(), DataCreator.generateCode(DataCreator.generateNumberRangeFourTo(9)), DataCreator.generateCode(DataCreator.generateNumberRangeFourTo(9)));
            databaseHelper.addNewUser(user.getNumber(), user.getFirstName(),user.getLastName());
            String code = DataCreator.generateCode(9);
            int cabinetNr = DataCreator.generateNumberOfCabinet();
            databaseHelper.addNewWire(code, cabinetNr , i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.menuMainButtonExit) {
            this.finish();
        } if(view.getId() == R.id.menuMainButtonCabinets){
            Intent intent = new Intent(this, CabinetChoose.class);
            startActivity(intent);
        } if(view.getId() == R.id.menuMainButtonFindWire){
            Intent intent = new Intent(this, WireSearch.class);
            startActivity(intent);
        }
    }

}