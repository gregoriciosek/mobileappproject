package pl.vashrax.myappapm.utils;

import java.util.Random;

public class DataCreator {

    public static String generateCode(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

    public static String generateNumber() {
        int min = 500_000_000;
        int max = 999_999_999;
        return String.valueOf((int) (Math.floor(Math.random() * (max - min)) + min));
    }

    public static int generateNumberRangeFourTo(int rangeTo) {
        int min = 4;
        int max = rangeTo;
        return (int) (Math.floor(Math.random() * (max - min)) + min);
    }

    public static int generateNumberOfCabinet() {
        int min = 1;
        int max = 6;
        int result = (int) (Math.floor(Math.random() * (max - min)) + min);
        if (result == 6)
            result = 5;
        else if (result == 0)
            result = 1;
        return result;
    }

}
