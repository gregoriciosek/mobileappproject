package pl.vashrax.myappapm.utils;

public class VariableContainer {

    public final static String IS_NEW = "com.example.myapp.IS_NEW";
    public final static String WIRE_CODE = "com.example.myapp.WIRE_CODE";
    public final static String WIRE_ID = "com.example.myapp.WIRE_ID";
    public final static String USER_NUMBER = "com.example.myapp.USER_NUMBER";
    public final static String USER_FIRST_NAME = "com.example.myapp.USER_FIRST_NAME";
    public final static String USER_LAST_NAME = "com.example.myapp.USER_LAST_NAME";
    public final static String CABINET_CODE = "com.example.myapp.CABINET_CODE";
    public final static String CABINET_ID = "com.example.myapp.CABINET_ID";


}
