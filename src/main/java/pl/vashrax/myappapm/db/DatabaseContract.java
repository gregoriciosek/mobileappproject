package pl.vashrax.myappapm.db;

import android.provider.BaseColumns;

public final class DatabaseContract {

    private DatabaseContract(){}

    public static class DatabaseTableWires implements BaseColumns {
        public static final String TABLE_NAME = "wires";
        public static final String COLUMN_ID = "wire_id";
        public static final String COLUMN_CODE ="wire_code";
        public static final String COLUMN_USER ="wire_user_id";
        public static final String COLUMN_CABINET ="wire_cabinet_id";
    }

    public static class DatabaseTableUsers implements BaseColumns {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_ID = "user_id";
        public static final String COLUMN_NUMBER ="user_number";
        public static final String COLUMN_FIRST_NAME ="user_first_name";
        public static final String COLUMN_LAST_NAME ="user_last_name";
    }

    public static class DatabaseTableCabinets implements BaseColumns {
        public static final String TABLE_NAME = "cabinets";
        public static final String COLUMN_ID = "cabinet_id";
        public static final String COLUMN_CODE ="cabinet_code";
    }
}
