package pl.vashrax.myappapm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import pl.vashrax.myappapm.db.DatabaseContract.DatabaseTableCabinets;
import pl.vashrax.myappapm.db.DatabaseContract.DatabaseTableUsers;
import pl.vashrax.myappapm.db.DatabaseContract.DatabaseTableWires;
import pl.vashrax.myappapm.model.User;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Inw_Oka_Str.db";

    private static final String SQL_CREATE_ENTITIES_WIRES =
            "CREATE TABLE IF NOT EXISTS " + DatabaseTableWires.TABLE_NAME + "(" +
                    DatabaseTableWires.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    DatabaseTableWires.COLUMN_CODE + " TEXT," +
                    DatabaseTableWires.COLUMN_USER + " INTEGER," +
                    DatabaseTableWires.COLUMN_CABINET + " INTEGER)";


    private static final String SQL_DELETE_ENTITIES_WIRES =
            "DROP TABLE IF EXISTS " + DatabaseTableWires.TABLE_NAME;

    private static final String SQL_CREATE_ENTITIES_USERS =
            "CREATE TABLE IF NOT EXISTS " + DatabaseTableUsers.TABLE_NAME + " (" +
                    DatabaseTableUsers.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    DatabaseTableUsers.COLUMN_NUMBER + " TEXT," +
                    DatabaseTableUsers.COLUMN_FIRST_NAME + " TEXT,"+
                    DatabaseTableUsers.COLUMN_LAST_NAME + " TEXT)";

    private static final String SQL_DELETE_ENTITIES_USERS =
            "DROP TABLE IF EXISTS " + DatabaseTableUsers.TABLE_NAME;

    private static final String SQL_CREATE_ENTITIES_CABINETS =
            "CREATE TABLE IF NOT EXISTS " + DatabaseTableCabinets.TABLE_NAME + " (" +
                    DatabaseTableCabinets.COLUMN_ID + " INTEGER PRIMARY KEY," +
                    DatabaseTableCabinets.COLUMN_CODE + " TEXT)";

    private static final String SQL_DELETE_ENTITIES_CABINETS =
            "DROP TABLE IF EXISTS " + DatabaseTableCabinets.TABLE_NAME;

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTITIES_WIRES);
        sqLiteDatabase.execSQL(SQL_DELETE_ENTITIES_USERS);
        sqLiteDatabase.execSQL(SQL_DELETE_ENTITIES_CABINETS);
        createTables();
    }

    public void createTables(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(SQL_CREATE_ENTITIES_CABINETS);
        db.execSQL(SQL_CREATE_ENTITIES_USERS);
        db.execSQL(SQL_CREATE_ENTITIES_WIRES);
    }

    public void deleteTables(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTITIES_WIRES);
        db.execSQL(SQL_DELETE_ENTITIES_USERS);
        db.execSQL(SQL_DELETE_ENTITIES_CABINETS);
    }

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Users
     */

    public void deleteUser(String number){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DatabaseTableUsers.TABLE_NAME, DatabaseTableUsers.COLUMN_NUMBER + "=?", new String[]{number});
    }

    public void addNewUser(String number, String firstName, String lastName) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseTableUsers.COLUMN_NUMBER, number);
        values.put(DatabaseTableUsers.COLUMN_FIRST_NAME, firstName);
        values.put(DatabaseTableUsers.COLUMN_LAST_NAME, lastName);
        db.insert(DatabaseTableUsers.TABLE_NAME, null, values);
    }

    public void updateUser(String number, String firstName, String lastName) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseTableUsers.COLUMN_NUMBER, number);
        values.put(DatabaseTableUsers.COLUMN_FIRST_NAME, firstName);
        values.put(DatabaseTableUsers.COLUMN_LAST_NAME,lastName);
        db.update(DatabaseTableUsers.TABLE_NAME, values, DatabaseTableUsers.COLUMN_NUMBER + "=?", new String[]{number});
    }

    public int getUserId(String number) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableUsers.COLUMN_ID };
        Cursor cursor = db.query(DatabaseTableUsers.TABLE_NAME, columns, DatabaseTableUsers.COLUMN_NUMBER + "=?", new String[]{number}, null, null, null);
        long nr = 0;
        while(cursor.moveToNext()){
            nr = cursor.getLong(0);
        }
        return (int)nr;
    }

    public User findUserById(int id) {
        Cursor cursor = findAllFromUsersWhereId(id);
        while(cursor.moveToNext()){
            long userId = cursor.getInt(0);
            String userNumber = cursor.getString(1);
            String userFirstName = cursor.getString(2);
            String userLastName = cursor.getString(3);
            User user = new User(userId,userNumber,userFirstName,userLastName);
            return user;
        }
        return null;
    }

    /**
     * Wires
     */

    public void deleteWire(String code){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DatabaseTableWires.TABLE_NAME, DatabaseTableWires.COLUMN_CODE + "=?", new String[]{code});
    }

    public void addNewWire(String code, int cabinetId, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseTableWires.COLUMN_CODE, code);
        values.put(DatabaseTableWires.COLUMN_CABINET, cabinetId);
        values.put(DatabaseTableWires.COLUMN_USER, userId);
        db.insert(DatabaseTableWires.TABLE_NAME, null, values);
        System.out.println("Dodano nowy wpis do szafy " + cabinetId);
    }

    public boolean isWireWithUser(int userId) {
        SQLiteDatabase db = getReadableDatabase();
        boolean result = false;
        String[] columns = { DatabaseTableWires.COLUMN_ID, DatabaseTableWires.COLUMN_USER};
        Cursor cursor = db.query(DatabaseTableWires.TABLE_NAME, columns, DatabaseTableWires.COLUMN_USER + "=?", new String[]{String.valueOf(userId)}, null, null, null);
        while(cursor.moveToNext()){
            return true;
        }
        return false;
    }



    /**
     * Cabinets
     */

    public void addNewCabinet(String code) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseTableCabinets.COLUMN_CODE, code);
        db.insert(DatabaseTableCabinets.TABLE_NAME, null, values);
    }

    /**
     * Select
     */

    public Cursor findAllFromCabinets()
    {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableCabinets.COLUMN_ID, DatabaseTableCabinets.COLUMN_CODE};
        Cursor cursor = db.query(DatabaseTableCabinets.TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }

    public Cursor findAllFromUsers()
    {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableUsers.COLUMN_ID, DatabaseTableUsers.COLUMN_NUMBER, DatabaseTableUsers.COLUMN_FIRST_NAME, DatabaseTableUsers.COLUMN_LAST_NAME};
        Cursor cursor = db.query(DatabaseTableUsers.TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }

    public Cursor findAllFromWires()
    {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableWires.COLUMN_ID, DatabaseTableWires.COLUMN_CODE, DatabaseTableWires.COLUMN_CABINET, DatabaseTableWires.COLUMN_USER};
        Cursor cursor = db.query(DatabaseTableWires.TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }

    public Cursor findAllFromWiresWhereCode(String code) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableWires.COLUMN_ID, DatabaseTableWires.COLUMN_CODE, DatabaseTableWires.COLUMN_CABINET, DatabaseTableWires.COLUMN_USER};
        Cursor cursor = db.query(DatabaseTableWires.TABLE_NAME, columns, DatabaseTableWires.COLUMN_CODE + "=?", new String[]{code}, null, null, null);
        return cursor;
    }

    public Cursor findAllFromUsersWhereId(int id) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableUsers.COLUMN_ID, DatabaseTableUsers.COLUMN_NUMBER, DatabaseTableUsers.COLUMN_FIRST_NAME, DatabaseTableUsers.COLUMN_LAST_NAME};
        Cursor cursor = db.query(DatabaseTableUsers.TABLE_NAME, columns, DatabaseTableUsers.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);
        return cursor;
    }

    public Cursor findAllFromCabinetsWhereId(int id) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { DatabaseTableCabinets.COLUMN_ID, DatabaseTableCabinets.COLUMN_CODE};
        Cursor cursor = db.query(DatabaseTableCabinets.TABLE_NAME, columns, DatabaseTableCabinets.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);
        return cursor;
    }

}
